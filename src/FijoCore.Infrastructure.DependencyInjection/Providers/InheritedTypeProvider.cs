using System;
using System.Linq;
using Ninject.Activation;

namespace FijoCore.Infrastructure.DependencyInjection.Providers {
	// ToDo unfinished
	public class InheritedTypeProvider<T> : Provider<T> {
		protected readonly Type ToType;
		public InheritedTypeProvider(Type toType) {
			ToType = toType;
		}

		protected override T CreateInstance(IContext context) {
			var service = context.Request.Service;
			if(service.GetGenericTypeDefinition() == ToType)
				return (T) (ToType.MakeGenericType(service.GetGenericArguments().ToArray())).GetConstructor(new Type[0]).Invoke(null);
			throw new NotSupportedException();
		}
	}
}