﻿using System.Diagnostics;
using System.Linq;
using FijoCore.Infrastructure.DependencyInjection.Resolvers;
using JetBrains.Annotations;
using Ninject.Planning.Bindings.Resolvers;
using Ninject.Syntax;

namespace FijoCore.Infrastructure.DependencyInjection.Extentions.IKernel {
	[PublicAPI]
	public static class BindAllExtention {
		[NotNull, Pure, DebuggerStepThrough, PublicAPI]
		public static IBindingToSyntax<T> BindAll<T>([NotNull] this Ninject.IKernel kernel) {
			if(kernel.Components.GetAll<IBindingResolver>().All(x => typeof(InheritedTypeResolver<T>) != x.GetType()))
				kernel.Components.Add<IBindingResolver, InheritedTypeResolver<T>>();
			return kernel.Bind<T>();
		}
	}
}