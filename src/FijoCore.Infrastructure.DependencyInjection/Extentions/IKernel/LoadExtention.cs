using System.ComponentModel;
using System.Diagnostics;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Interface;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Module.Stock;
using JetBrains.Annotations;
using GIKernel = FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface.IKernel;

namespace FijoCore.Infrastructure.DependencyInjection.Extentions.IKernel {
	public static class LoadExtention
	{
		[NotNull] private static readonly AdditionalModuleStock AdditionalModuleStock = new AdditionalModuleStock();

		[DebuggerStepThrough, PublicAPI, Description("Loads an additional module outside of your init kernel, in a module. Use this only in your modules AddModule methode.")]
		public static void Load([NotNull] this GIKernel me, [NotNull] IExtendedNinjectModule module) {
			AdditionalModuleStock.Store(me, module);
		}
		
		[DebuggerStepThrough, PublicAPI, Description("Load many additional modules outside of your init kernel, in a module. Use this only in your modules AddModule methode.")]
		public static void Load([NotNull] this GIKernel me, params IExtendedNinjectModule[] modules) {
			#region PreCondition
			Debug.Assert(modules != null, "modules != null");
			#endregion
// ReSharper disable AssignNullToNotNullAttribute
			foreach(var module in modules) me.Load(module);
// ReSharper restore AssignNullToNotNullAttribute
		}
	}
}