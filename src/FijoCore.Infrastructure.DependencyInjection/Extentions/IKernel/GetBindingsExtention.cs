﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using JetBrains.Annotations;
using Ninject;
using Ninject.Infrastructure;
using Ninject.Planning.Bindings;

namespace FijoCore.Infrastructure.DependencyInjection.Extentions.IKernel
{
	public static class GetBindingsExtention
	{
		[NotNull] private static readonly FieldInfo BindingsField = typeof (KernelBase).GetField("bindings", BindingFlags.NonPublic | BindingFlags.Instance);
		
		[NotNull, DebuggerStepThrough]
		public static IEnumerable<Type> GetBindings([NotNull] this KernelBase me) {
// ReSharper disable PossibleNullReferenceException
// ReSharper disable AssignNullToNotNullAttribute
			return ((Multimap<Type, IBinding>)BindingsField.GetValue(me)).Keys;
// ReSharper restore AssignNullToNotNullAttribute
// ReSharper restore PossibleNullReferenceException
		}
	}
}
