﻿using System;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.DependencyInjection.Exceptions {
	[PublicAPI, Serializable]
	public class DependencyInjectionException : Exception {
		public DependencyInjectionException() {}

		public DependencyInjectionException(string message) : base(message) {}

		public DependencyInjectionException(string message, Exception exception) : base(message, exception) {}
	}
}