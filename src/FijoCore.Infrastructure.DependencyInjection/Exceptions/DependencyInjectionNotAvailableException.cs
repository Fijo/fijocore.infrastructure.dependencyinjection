using System;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.DependencyInjection.Exceptions {
	[PublicAPI, Serializable]
	public class DependencyInjectionNotAvailableException : DependencyInjectionException {
		public DependencyInjectionNotAvailableException() {}

		public DependencyInjectionNotAvailableException(string message) : base(message) {}

		public DependencyInjectionNotAvailableException(string message, Exception exception) : base(message, exception) {}
	}
}