﻿using System;
using System.Diagnostics;
using FijoCore.Infrastructure.DependencyInjection.Exceptions;
using JetBrains.Annotations;
using Ninject;
using IKernel = FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface.IKernel;

namespace FijoCore.Infrastructure.DependencyInjection.InitKernel
{
    public static class Kernel {
    	[CanBeNull] private static IKernel _inject;

    	internal static bool InjectionAvalible { get { return _inject != null; } }

    	[NotNull, PublicAPI]
    	public static IKernel Inject {
    		get {
    			if (!InjectionAvalible) HandleInjectionNotAvailable();
// ReSharper disable AssignNullToNotNullAttribute
    			return _inject;
// ReSharper restore AssignNullToNotNullAttribute
    		}
    		internal set { _inject = value; }
    	}

		[TerminatesProgram]
    	private static void HandleInjectionNotAvailable() {
    		var fakedInnerException = new NullReferenceException();
    		throw new DependencyInjectionNotAvailableException(fakedInnerException.Message, fakedInnerException);
    	}

    	[NotNull, DebuggerStepThrough, PublicAPI]
        public static T Resolve<T>() {
			HandleResolve<T>();
// ReSharper disable AssignNullToNotNullAttribute
			return Inject.Get<T>();
// ReSharper restore AssignNullToNotNullAttribute
        }
		
		[DebuggerStepThrough]
		private static void HandleResolve<T>() {
			if(OnResolve != null) OnResolve(typeof(T));
		}

    	[CanBeNull] internal static Action<Type> OnResolve;
    }
}