using System.Collections.Generic;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Interface;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;

namespace FijoCore.Infrastructure.DependencyInjection.InitKernel.Base {
	public abstract class ExtendedNinjectModule : IExtendedNinjectModule {
		private IKernel _kernel;
		private readonly string _name;

		protected ExtendedNinjectModule() {
			_name = GetType().Name;
		}

		#region Implementation of IExtendedNinjectModule
		public IKernel Kernel { get { return _kernel; } }
		public virtual void InitModule() {}

		public virtual void AddModule(IKernel kernel) {}

		public void OnLoadKernel(IKernel kernel) {
			_kernel = kernel;
		}

		public virtual void OnLoad(IKernel kernel) {}

		public virtual void OnUnload(IKernel kernel) {}
		public IDictionary<IExtendedInitHandler, object> ExtendedInit { get; protected set; }
		public string Name { get { return _name; } }
		public virtual void Init(IKernel kernel) {}
		public virtual void RegisterHandlers(IKernel kernel) {}
		public virtual void Validate() {}
		public virtual void Loaded(IKernel kernel) {}
		#endregion
	}
}