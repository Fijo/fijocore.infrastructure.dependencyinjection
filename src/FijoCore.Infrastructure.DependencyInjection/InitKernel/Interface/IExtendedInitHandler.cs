using JetBrains.Annotations;
using IKernel = FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface.IKernel;

namespace FijoCore.Infrastructure.DependencyInjection.InitKernel.Interface {
	public interface IExtendedInitHandler {
		void Handle([NotNull] IKernel kernel, [NotNull] IExtendedNinjectModule module, object obj);
	}
}