using System.Collections.Generic;
using JetBrains.Annotations;
using IKernel = FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface.IKernel;

namespace FijoCore.Infrastructure.DependencyInjection.InitKernel.Interface {
	public interface IExtendedNinjectModule {
		IKernel Kernel { get; }
		void InitModule();
		void AddModule([NotNull] IKernel kernel);
		void OnLoadKernel([NotNull] IKernel kernel);
		void OnLoad([NotNull] IKernel kernel);
		void OnUnload([NotNull] IKernel kernel);
		IDictionary<IExtendedInitHandler, object> ExtendedInit { get; }
		[NotNull]
		string Name { get; }
		void Init([NotNull] IKernel kernel);
		void RegisterHandlers([NotNull] IKernel kernel);
		void Validate();
		void Loaded([NotNull] IKernel kernel);
	}
}