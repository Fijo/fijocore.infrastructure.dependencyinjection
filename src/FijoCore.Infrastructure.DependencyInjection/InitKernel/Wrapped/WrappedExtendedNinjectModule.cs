using System.Collections.Generic;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Interface;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;
using JetBrains.Annotations;
using Ninject.Infrastructure;

namespace FijoCore.Infrastructure.DependencyInjection.InitKernel.Wrapped {
	public class WrappedExtendedNinjectModule : IWrappedExtendedNinjectModule {
		[NotNull] private readonly IExtendedNinjectModule _extendedNinjectModule;

		public WrappedExtendedNinjectModule([NotNull] IExtendedNinjectModule extendedNinjectModule) {
			_extendedNinjectModule = extendedNinjectModule;
		}

		#region Implementation of IHaveKernel
		Ninject.IKernel IHaveKernel.Kernel { get { return Kernel; } }

		public void OnLoad(IKernel kernel) {}

		public void OnInternalLoad(IKernel kernel) {
			_extendedNinjectModule.OnLoad(kernel);
		}

		public void OnUnload(IKernel kernel) {
			_extendedNinjectModule.OnUnload(kernel);
		}

		public IDictionary<IExtendedInitHandler, object> ExtendedInit { get { return _extendedNinjectModule.ExtendedInit; } }

		public void AddModule(IKernel kernel) {
			_extendedNinjectModule.AddModule(kernel);
		}

		public void Init(IKernel kernel) {
			_extendedNinjectModule.Init(kernel);
		}

		public void RegisterHandlers(IKernel kernel) {
			_extendedNinjectModule.RegisterHandlers(kernel);
		}

		public void Validate() {
			_extendedNinjectModule.Validate();
		}

		public void Loaded(IKernel kernel) {
			_extendedNinjectModule.Loaded(kernel);
		}
		#endregion

		#region Implementation of INinjectModule
		public void OnLoad([NotNull] Ninject.IKernel kernel) {
			OnLoad((IKernel) kernel);
		}

		public void OnUnload([NotNull] Ninject.IKernel kernel) {
			OnUnload((IKernel) kernel);
		}

		[NotNull]
		public string Name { get { return _extendedNinjectModule.Name; } }
		#endregion

		#region Implementation of IWrappedExtendedNinjectModule
		public IKernel Kernel { get { return _extendedNinjectModule.Kernel; } }
		#endregion
	}
}