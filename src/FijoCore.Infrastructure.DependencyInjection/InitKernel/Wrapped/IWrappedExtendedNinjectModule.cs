using System.Collections.Generic;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Interface;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;
using JetBrains.Annotations;
using Ninject.Modules;

namespace FijoCore.Infrastructure.DependencyInjection.InitKernel.Wrapped
{
	[PublicAPI]
	public interface IWrappedExtendedNinjectModule : INinjectModule
	{
		[PublicAPI] new IKernel Kernel { get; }
		[PublicAPI] void OnLoad([NotNull] IKernel kernel);
		[PublicAPI] void OnUnload([NotNull] IKernel kernel);
		[PublicAPI] void AddModule([NotNull] IKernel kernel);
		[PublicAPI] void OnInternalLoad([NotNull] IKernel kernel);
		[PublicAPI] IDictionary<IExtendedInitHandler, object> ExtendedInit { get; }
		[PublicAPI] void Init([NotNull] IKernel kernel);
		[PublicAPI] void RegisterHandlers([NotNull] IKernel kernel);
		[PublicAPI] void Validate();
		[PublicAPI] void Loaded([NotNull] IKernel kernel);
	}
}