﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.DependencyInjection.Extentions.IKernel;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Init.Interface;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Interface;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Module.Stock;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Wrapped;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Module;
using JetBrains.Annotations;
using Ninject;
using IKernel = FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface.IKernel;

namespace FijoCore.Infrastructure.DependencyInjection.InitKernel.Init {

	public class InitKernel : IInitKernel {
		#region Fields
		[NotNull] private readonly IList<IExtendedNinjectModule> _modules = new List<IExtendedNinjectModule>();
		[NotNull] private readonly AdditionalModuleStock _additionalModuleStock = new AdditionalModuleStock();
		[CanBeNull, PublicAPI] public Action<Type> OnResolve;
		#endregion

		#region LoadModule
		[DebuggerStepThrough, PublicAPI]
		protected void LoadModule([NotNull] IExtendedNinjectModule module) {
			lock (_modules) InternalLoadModule(module);
		}

		[DebuggerStepThrough, Desc("_modules must be locked when before use this function")]
		private void InternalLoadModule([NotNull] IExtendedNinjectModule module) {
			#region PreCondition
			Debug.Assert(!_modules.Contains(module));
			#endregion
			var type = module.GetType();
			if(_modules.Any(x => x.GetType() == type)) return;
			Trace.WriteLine(string.Format("Load/ Add Module ´{0}´", module.Name));
			InitModule(module);
			_modules.Insert(0, module);
		}

		[DebuggerStepThrough]
		protected void LoadModules([NotNull] params IExtendedNinjectModule[] modules) {
			lock (_modules) {
				// ReSharper disable AssignNullToNotNullAttribute
				foreach (var module in modules) InternalLoadModule(module);
				// ReSharper restore AssignNullToNotNullAttribute
			}
		}

		private void InitModule(IExtendedNinjectModule module) {
			module.InitModule();
		}
		#endregion

		[DebuggerStepThrough, Conditional("ReInject")]
		private void UnloadKernel() {
			using (var kernel = Kernel.Inject) {
				// ReSharper disable AssignNullToNotNullAttribute
				// ReSharper disable PossibleNullReferenceException
				foreach (var moduleName in kernel.GetModules().Select(x => x.Name)) kernel.Unload(moduleName);
				// ReSharper restore PossibleNullReferenceException
				foreach (var binding in ((KernelBase) kernel).GetBindings().ToArray()) kernel.Unbind(binding);
				// ReSharper restore AssignNullToNotNullAttribute
			}
			Kernel.OnResolve = null;
		}

		[DebuggerStepThrough]
		public virtual void Init() {
			lock (typeof (InitKernel)) {
				if (Kernel.InjectionAvalible) {
					Debug.WriteLine("Injection is already inited!");
					UnloadKernel();
				}
				Kernel.OnResolve = OnResolve;
				var kernel = Kernel.Inject = new InjectionKernel(_modules.Select(WrapModule));
				HandleLoadCycle(kernel);
			}
		}

		private void HandleLoadCycle([NotNull] IKernel kernel) {
			HandleAllAddModules(kernel);
			HandleLoad(kernel);
			HandleInit(kernel);
			HandleRegisterHandlers(kernel);
			HandleExtendedInitHandlers(kernel);
			HandleLoaded(kernel);
			Validate();
		}

		private void HandleAllAddModules([NotNull] IKernel kernel) {
			// ReSharper disable AssignNullToNotNullAttribute
			foreach (var currentEntryModule in _modules.ToArray()) HandleAddModuleForSubModule(kernel, currentEntryModule);
			// ReSharper restore AssignNullToNotNullAttribute
			_additionalModuleStock.Remove(kernel);
		}

		private void HandleAddModuleForSubModule([NotNull] IKernel kernel, [NotNull] IExtendedNinjectModule currentEntryModule) {
			HandleAddModule(kernel, currentEntryModule);
			while (true) {
				var module = _additionalModuleStock.Get(kernel);
				if (module == null) break;
				LoadModule(module);
				HandleAddModule(kernel, module);
			}
		}

		[NotNull]
		private WrappedExtendedNinjectModule WrapModule([NotNull] IExtendedNinjectModule module) {
			return new WrappedExtendedNinjectModule(module);
		}

		private void HandleLoaded([NotNull] IKernel kernel) {
			foreach (var module in _modules) module.Loaded(kernel);
		}

		private void HandleAddModule([NotNull] IKernel kernel, [NotNull] IExtendedNinjectModule module) {
			module.AddModule(kernel);
		}

		private void HandleLoad([NotNull] IKernel kernel) {
			// ReSharper disable PossibleNullReferenceException
			foreach (var module in _modules) {
				module.OnLoadKernel(kernel);
				module.OnLoad(kernel);
			}
			// ReSharper restore PossibleNullReferenceException
		}

		private void HandleInit([NotNull] IKernel kernel) {
			// ReSharper disable PossibleNullReferenceException
			foreach (var module in _modules) module.Init(kernel);
			// ReSharper restore PossibleNullReferenceException
		}

		private void HandleRegisterHandlers([NotNull] IKernel kernel) {
			// ReSharper disable PossibleNullReferenceException
			foreach (var module in _modules) module.Init(kernel);
			// ReSharper restore PossibleNullReferenceException
		}

		private void HandleExtendedInitHandlers([NotNull] IKernel kernel) {
			// ReSharper disable PossibleNullReferenceException
			foreach (var module in _modules.Where(module => !Equals(module.ExtendedInit, null)))
				foreach (var extendedInjectHandler in module.ExtendedInit)
					extendedInjectHandler.Key.Handle(kernel, module, extendedInjectHandler.Value);
			// ReSharper restore PossibleNullReferenceException
		}

		#region Validate
		[Conditional("DEBUG")]
		private void Validate() {
			// ReSharper disable PossibleNullReferenceException
			foreach (var module in _modules) module.Validate();
			// ReSharper restore PossibleNullReferenceException
		}
		#endregion

		public static Action<Type> ResolveDebug { get { return x => Trace.WriteLine(string.Format("Injection Resolve {0}", x.Name)); } }
	}
}