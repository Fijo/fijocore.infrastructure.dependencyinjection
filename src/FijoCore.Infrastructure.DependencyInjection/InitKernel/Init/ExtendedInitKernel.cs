using FijoCore.Infrastructure.DependencyInjection.InitKernel.Init.Interface;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.DependencyInjection.InitKernel.Init
{
	[PublicAPI]
    public abstract class ExtendedInitKernel : InitKernel, IExtendedInitKernel
    {
        public abstract void PreInit();

        public override void Init()
        {
        	lock (this) {
        		PreInit();
        		base.Init();
        	}
        }
    }
}