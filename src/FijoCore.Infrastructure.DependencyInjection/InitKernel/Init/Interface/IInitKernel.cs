using JetBrains.Annotations;

namespace FijoCore.Infrastructure.DependencyInjection.InitKernel.Init.Interface
{
	[PublicAPI]
    public interface IInitKernel
    {
        [PublicAPI] void Init();
    }
}