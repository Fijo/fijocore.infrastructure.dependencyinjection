using JetBrains.Annotations;

namespace FijoCore.Infrastructure.DependencyInjection.InitKernel.Init.Interface
{
	[PublicAPI]
    public interface IExtendedInitKernel : IInitKernel
    {
		[PublicAPI] void PreInit();
    }
}