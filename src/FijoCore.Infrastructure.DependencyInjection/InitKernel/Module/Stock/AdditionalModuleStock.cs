using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Interface;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.DependencyInjection.InitKernel.Module.Stock {
	public class AdditionalModuleStock {
		[NotNull, PublicAPI] protected static readonly IDictionary<IKernel, IList<IExtendedNinjectModule>> Module = new Dictionary<IKernel, IList<IExtendedNinjectModule>>();

		public void Store([NotNull] IKernel key, [NotNull] IExtendedNinjectModule module) {
			var modules = GetCurrentModules(key);
			modules.Add(module);
		}

		[NotNull]
		private IList<IExtendedNinjectModule> GetCurrentModules([NotNull] IKernel key) {
			IList<IExtendedNinjectModule> modules;
			if (!Module.TryGetValue(key, out modules)) Module[key] = modules = new List<IExtendedNinjectModule>();
			Debug.Assert(modules != null, "modules != null");
// ReSharper disable AssignNullToNotNullAttribute
			return modules;
// ReSharper restore AssignNullToNotNullAttribute
		}

		[CanBeNull]
		public IExtendedNinjectModule Get([NotNull] IKernel key) {
			var modules = GetCurrentModules(key);
			if(!modules.Any()) return null;
			var valueToReturn = modules.First();
			modules.RemoveAt(0);
			return valueToReturn;
		}

		public void Remove([NotNull] IKernel key) {
			Module.Remove(key);
		}
	}
}