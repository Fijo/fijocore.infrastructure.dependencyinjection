﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using Ninject.Components;
using Ninject.Infrastructure;
using Ninject.Planning.Bindings;
using Ninject.Planning.Bindings.Resolvers;

namespace FijoCore.Infrastructure.DependencyInjection.Resolvers {
	[UsedImplicitly]
	public class InheritedTypeResolver<T> : NinjectComponent, IBindingResolver {
		#region Implementation of IBindingResolver
		public IEnumerable<IBinding> Resolve(Multimap<Type, IBinding> bindings, Type service) {
			return typeof (T).IsAssignableFrom(service) && typeof (T) != service
				       ? bindings[typeof (T)]
				       : Enumerable.Empty<IBinding>();
		}
		#endregion
	}
}